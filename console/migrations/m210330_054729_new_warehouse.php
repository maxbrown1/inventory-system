<?php

use yii\db\Migration;

/**
 * Class m210330_054729_new_warehouse
 */
class m210330_054729_new_warehouse extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%warehouse}}', [
        'id' => $this->primaryKey(),
        'code' => $this->integer(11)->notNull()->unique()->comment('Номер склада'),
        'name' => $this->string(150)->notNull()->comment('Наименование склада'),

        'status' => $this->smallInteger(2)->notNull()->defaultValue(\frontend\models\Warehouse::STATUS_ACTIVE)->comment('Статус склада'),
        'created_at' => $this->timestamp()->comment('Дата-время создания записи'),
        'updated_at' => $this->timestamp()->comment('Дата-время последнего изменения'),
      ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210330_054729_new_warehouse cannot be reverted.\n";

        return false;
    }

}
