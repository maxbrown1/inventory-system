<?php

use yii\db\Migration;

/**
 * Class m210405_092940_create_index_from_product
 */
class m210405_092940_create_index_from_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createIndex(
        'idx-product-name',
        'product',
        'name'
      );
      $this->createIndex(
        'idx-product-date_manufacture',
        'product',
        'date_manufacture'
      );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210405_092940_create_index_from_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210405_092940_create_index_from_product cannot be reverted.\n";

        return false;
    }
    */
}
