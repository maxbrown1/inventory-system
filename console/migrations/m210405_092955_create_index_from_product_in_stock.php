<?php

use yii\db\Migration;

/**
 * Class m210405_092955_create_index_from_product_in_stock
 */
class m210405_092955_create_index_from_product_in_stock extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createIndex(
        'idx-product_in_stock-warehouse_id',
        'product_in_stock',
        'warehouse_id'
      );
      $this->createIndex(
        'idx-product_in_stock-product_id',
        'product_in_stock',
        'product_id'
      );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210405_092955_create_index_from_product_in_stock cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210405_092955_create_index_from_product_in_stock cannot be reverted.\n";

        return false;
    }
    */
}
