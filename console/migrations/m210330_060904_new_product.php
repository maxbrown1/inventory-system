<?php

use yii\db\Migration;

/**
 * Class m210330_060904_new_product
 */
class m210330_060904_new_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%product}}', [
        'id' => $this->primaryKey(),
        'name' => $this->string(150)->notNull()->comment('Наименование товара'),
        'description' => $this->text()->null()->comment('Описание товара'),
        'date_manufacture' => $this->date()->notNull()->comment('Дата изготовления'),

        'status' => $this->smallInteger(2)->notNull()->defaultValue(10)->comment('Статус склада'),
        'created_at' => $this->timestamp()->comment('Дата/время создания записи'),
        'updated_at' => $this->timestamp()->comment('Дата/время последнего изменения'),
      ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210330_060904_new_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210330_060904_new_product cannot be reverted.\n";

        return false;
    }
    */
}
