<?php

use yii\db\Migration;

/**
 * Class m210330_063857_new_product_in_stock
 */
class m210330_063857_new_product_in_stock extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%product_in_stock}}', [
        'id' => $this->primaryKey(),
        'warehouse_id' => $this->integer(11)->notNull()->comment('ID Склада'),
        'product_id' => $this->integer(11)->notNull()->comment('ID Товара'),
        'cost' => $this->decimal(11,2)->null()->comment('Стоимость товара на складе'),
        'count' => $this->integer(11)->null()->comment('Кол-во товаров на складе'),

        'created_at' => $this->timestamp()->comment('Дата-время создания записи'),
        'updated_at' => $this->timestamp()->comment('Дата-время последнего изменения'),
      ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210330_063857_new_product_in_stock cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210330_063857_new_product_in_stock cannot be reverted.\n";

        return false;
    }
    */
}
