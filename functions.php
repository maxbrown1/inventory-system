<?php
function debug($arr)
{
    echo '<pre>' . print_r($arr, true) . '</pre>';
    die();
}
function request ()
{
  return Yii::$app->request;
}
function response ()
{
  return Yii::$app->response;
}
function session()
{
  return Yii::$app->session;
}
function identity()
{
  return Yii::$app->user->identity;
}
function transaction()
{
  return Yii::$app->db->beginTransaction();
}
function performAjaxValidation($model)
{
    if (request()->isAjax && request()->getBodyParam('ajax')) {
      response()->format = yii\web\Response::FORMAT_JSON;
      response()->data = \yii\bootstrap\ActiveForm::validate($model);
      response()->send();
      exit();
    }
}
function performAjaxMultipleValidation($model)
{
    if (request()->isAjax && request()->getBodyParam('ajax')) {
      response()->format = yii\web\Response::FORMAT_JSON;
      response()->data = \yii\bootstrap\ActiveForm::validateMultiple($model);
      response()->send();
      exit();
    }
}
