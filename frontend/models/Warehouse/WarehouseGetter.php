<?php
namespace frontend\models\Warehouse;

use yii\helpers\ArrayHelper;
use frontend\models\Warehouse;


/**
 * Класс получения данных о складе
 *
 * @package frontend\models\Warehouse\
 */
class WarehouseGetter
{
  /**
   * Получаем все склады
   *
   * @return array|\yii\db\ActiveRecord[]
   */
  public static function getAllWarehouse(){
    return Warehouse::find()->indexBy('id')->all();
  }

}