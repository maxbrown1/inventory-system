<?php
namespace frontend\models\Warehouse;

use frontend\models\ProductInStock\ProductInStockDeleter;
use frontend\models\Warehouse;


/**
 * Класс для удаления Склада
 *
 * @property Warehouse $warehouse - Склад
 * @package frontend\models\Warehouse
 */
class WarehouseDeleter
{
  /**
   * @var Warehouse
   */
  protected $warehouse;

  public function __construct(Warehouse $warehouse){
    $this->warehouse = $warehouse;
  }

  /**
   * Удаление склада и всех связанных товаров
   *
   * @throws \Throwable
   * @throws \yii\db\StaleObjectException
   */
  public function delete() {
    if($this->warehouse->delete()) {
      ProductInStockDeleter::deleteAllProductInStockToWarehouse($this->warehouse->id);
    }
  }
}