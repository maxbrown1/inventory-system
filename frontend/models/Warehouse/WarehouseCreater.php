<?php
namespace frontend\models\Warehouse;

use frontend\models\Warehouse;
use frontend\models\Warehouse\Form\WarehouseCreateForm;


/**
 * Класс сохранения Склада
 * @property WarehouseCreateForm $warehouse - Данные о складе с формы
 * @package frontend\models\Warehouse
 */
class WarehouseCreater
{
  /**
   * @var WarehouseCreateForm
   */
  protected $warehouse;

  public function __construct(WarehouseCreateForm $warehouse){
    $this->warehouse = $warehouse;
  }

  /**
   * Создание новой записи о складе
   *
   * @return bool
   */
  public function createWarehouse() {
    $record = new Warehouse();
    $record->code = $this->warehouse->code;
    $record->name = $this->warehouse->name;
    return $record->save();
  }

}