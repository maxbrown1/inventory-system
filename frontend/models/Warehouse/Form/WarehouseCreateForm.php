<?php
namespace frontend\models\Warehouse\Form;

use yii\base\Model;


/**
 * Форма создания Склада
 *
 * @property int $code - Код склада
 * @property int $name - Наименование склада
 * @package frontend\models\Warehouse\Form
 */
class WarehouseCreateForm extends Model
{
  /**
   * @var
   */
  public $code;
  /**
   * @var
   */
  public $name;

  public function attributeLabels()
  {
    return [
      'code' => 'Код склада',
      'name' => 'Наименование склада',
    ];

  }

  public function rules()
  {
    return [
      [['code', 'name'], 'required'],
      ['name', 'string', 'length' => [1, 150]],
      ['code', 'integer', 'min' => 1],
      ['code', 'unique', 'targetClass' => '\frontend\models\Warehouse', 'message' => 'Данный код склада уже используется']
    ];
  }

}