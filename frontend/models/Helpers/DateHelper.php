<?php
namespace frontend\models\Helpers;

/**
 * Хелпер для работы с датами
 *
 * @property string $date - Дата для форматирования
 * @package frontend\models\Helpers
 */
class DateHelper
{
  /**
   * @var \DateTime
   */
  public $date;

  public function __construct($string = false){
    $this->date = ($string) ? new \DateTime($string) : new \DateTime();
  }

  /**
   * Изменение формата даты в человекопонятный вид (01.12.2020 г.)
   *
   * @return string
   */
  public function formatDateHumanView() {
    return $this->date->format('d.m.Y').' г.';
  }

  /**
   * Изменение формата даты в формат для сохранения в БД
   *
   * @return mixed
   */
  public function formatDateForDb() {
    return $this->date->format('Y-m-d');
  }

}