<?php
namespace frontend\models\Helpers;


/**
 * Хелпер для работы с текстом
 *
 * @property string $text - Текст для форматирования
 * @package frontend\models\Helpers
 */
class TextHelper
{
  /**
   * @var string
   */
  public $text;

  public function __construct(string $text){
    $this->text = $text;
  }

  /**
   * Обрезка текста указанной длины, в конце обрезанной строки добавляется троеточие
   *
   * @param int $size_cut - Кол-во символов после которых будет обрезан текст
   * @return string
   */
  public function cutterText(int $size_cut) {
    $length = mb_strlen($this->text,'UTF-8');
    $string = mb_substr($this->text, 0, $size_cut,'UTF-8');
    return ($length > $size_cut) ? $string.'...' : $string;
  }

  /**
   * Отображение денег в человекопонятной форме (1 250,50 р.)
   *
   * @return string
   */
  public function formatMoneyHumanView() {
    return number_format($this->text, 2, ',', " ").' р.';
  }

  /**
   * Отображение стоимости и кол-ва товара по складам
   *
   * @param $warehouse_name - Наименование склада
   * @return string
   */
  public function warehouseAndPriceProduct($warehouse_name) {
    return $warehouse_name.' - '.$this->formatMoneyHumanView();
  }
}