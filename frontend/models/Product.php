<?php
namespace frontend\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;


class Product extends ActiveRecord
{
  /**
   * Статус записи - Активна
   * @var int
   */
  const STATUS_ACTIVE = 10;
  /**
   * Статус записи - Неактивна
   * @var int
   */
  const STATUS_INACTIVE = 0;
  /**
   * @var mixed|null
   */
  private $name;
  /**
   * @var mixed|null
   */
  private $description;
  /**
   * @var mixed|string|null
   */
  private $date_manufacture;
  /**
   * @var mixed|null
   */
  private $id;

  public static function tableName()
  {
    return '{{product}}';
  }

  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
        'value' => new Expression('NOW()'),
      ],
    ];
  }

}