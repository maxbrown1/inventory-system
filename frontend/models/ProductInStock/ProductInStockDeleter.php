<?php
namespace frontend\models\ProductInStock;

use frontend\models\ProductInStock;


/**
 * Удаление информации о наличии товара на складе
 *
 * @property $product_in_stock - товары на складе
 * @package frontend\models\ProductInStock
 */
class ProductInStockDeleter
{
  public $product_in_stock;

  public function __construct(ProductInStock $product_in_stock){
    $this->product_in_stock = $product_in_stock;
  }

  /**
   * Удаление информации о товаре на складе
   *
   * @return mixed
   */
  public function deleteProductInStock() {
    return $this->product_in_stock->delete();
  }

  /**
   * Удаление всех товаров связанных со складом, при удалении склада
   *
   * @param $warehouse_id - Cклад id
   * @throws \Throwable
   * @throws \yii\db\StaleObjectException
   */
  public static function deleteAllProductInStockToWarehouse($warehouse_id) {
    $models = ProductInStock::findAll(['warehouse_id' => $warehouse_id]);
    foreach ($models as $model) {
      $model->delete();
    }
  }

}