<?php
namespace frontend\models\ProductInStock;

use frontend\models\Product;
use frontend\models\ProductInStock;


/**
 * Сохранение данных о наличии товара на складе
 *
 * @property Product $product - Товар
 * @property array $product_in_stocks - Данные о наличии товара на складе
 * @package frontend\models\ProductInStock
 */
class ProductInStockCreater
{
  /**
   * @var Product
   */
    protected $product;
  /**
   * @var
   */
    protected $product_in_stocks;


    public function __construct(Product $product, $product_in_stocks){
      $this->product = $product;
      $this->product_in_stocks = $product_in_stocks;
    }

  /**
   * Сохранение информации о наличии товара на складе
   *
   * @return bool
   */
    public function saveProductInStock() {
      foreach ($this->product_in_stocks as $product_in_stock) {
        is_numeric($product_in_stock->id)
          ? $model = ProductInStock::findOne($product_in_stock->id)
          : $model = new ProductInStock();
        $model->warehouse_id = $product_in_stock->warehouse_id;
        $model->product_id = $this->product->id;
        $model->cost = empty($product_in_stock->cost) ? null : $product_in_stock->cost;
        $model->count = empty($product_in_stock->cost) ? null : $product_in_stock->count;
        if(!$model->save()) {
          return false;
        }
      }
      return true;
    }

}