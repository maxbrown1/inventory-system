<?php
namespace frontend\models\ProductInStock;

use frontend\models\ProductInStock;
use frontend\models\Warehouse;
use yii\helpers\ArrayHelper;
use frontend\models\ProductInStock\Form\ProductInStockCreateForm;


/**
 * Класс для получения данных о наличии товаров на складах
 *
 * @package frontend\models\ProductInStock
 */
class ProductInStockGetter
{
  /**
   * Получаем все модели для новой формы
   *
   * @param $warehouses - список складов
   * @return mixed
   * @throws \yii\base\InvalidConfigException
   */
  public function getAllModelsForCreateForm($warehouses) {
    foreach ($warehouses as $warehouse) {
      $models[] = \Yii::createObject(ProductInStockCreateForm::class)->populateModelToCreate($warehouse);
    }
    return $models;
  }

  /**
   * Получаем все модели для обновления формы
   *
   * @param $warehouses - список складов
   * @param $product_in_stocks - информация о наличии товаров на складах- информация о наличии товаров на складах
   * @return mixed
   * @throws \yii\base\InvalidConfigException
   */
  public function getAllModelsForUpdateForm($warehouses, $product_in_stocks) {
    foreach ($product_in_stocks as $product_in_stock) {
      $models[] = \Yii::createObject(ProductInStockCreateForm::class)->populateModelToUpdate($product_in_stock);
    }
    // если кол-во складов и кол-во прайсов не совпадает, то добавляем недостоющие склады в модель
    return (count($warehouses) !== count($product_in_stocks))
      ? $this->addMissingWarehouseInForm($warehouses, $product_in_stocks, $models)
      : $models;
  }

  /**
   * Добавляем недостоющие склады в форму
   *
   * Случается ситуация когда данные о товаре уже записаны в БД и после этого был добавлен новый склад.
   * Тогда при редактировании товара отобразятся только те склады которые были добавлены при создании, а новых не будет.
   * Данная функция решает эту проблему.
   *
   * @param $warehouses - список складов
   * @param $product_in_stocks - информация о наличии товаров на складах
   * @param $models - данные для формы со старыми и новыми складами
   * @return mixed
   * @throws \yii\base\InvalidConfigException
   */
  public function addMissingWarehouseInForm($warehouses, $product_in_stocks, $models) {
    $added_warehouse = ArrayHelper::getColumn($product_in_stocks, 'warehouse_id');
    $all_warehouse = ArrayHelper::getColumn($warehouses, 'id');
    $difference = array_diff($all_warehouse, $added_warehouse);
    foreach ($difference as $id) {
      $warehouse = Warehouse::findOne($id);
      $models[] = \Yii::createObject(ProductInStockCreateForm::class)->populateModelToCreate($warehouse);
    }
    return $models;
  }

  /**
   * Форматируем информация о наличии товаров на складах, для API
   *
   * @param $product - Товар
   * @param $warehouses - список складов
   * @return array
   */
  public function apiFormatProductInStock($product, $warehouses) {
    $product_in_stocks = $this->getAllProductInStockToProduct($product);
    $response = [];
    foreach ($product_in_stocks as $product_in_stock) {
      $item = [];
      $item['price']['stock_code'] = $warehouses[$product_in_stock->warehouse_id]->code;
      $item['price']['stock_name'] = $warehouses[$product_in_stock->warehouse_id]->name;
      $item['price']['price'] = ($product_in_stock->cost) ? $product_in_stock->cost : '0.00';
      $response[] = $item;
    }
    return $response;
  }

  /**
   * Получаем все позиции товаров на складах по товару
   *
   * @param $product
   * @return ProductInStock[]
   */
  public function getAllProductInStockToProduct($product) {
    return ProductInStock::findAll(['product_id' => $product->id]);
  }

}