<?php
namespace frontend\models\ProductInStock\Form;

use frontend\models\Warehouse;
use yii\base\Model;
use frontend\models\ProductInStock;


/**
 * Форма для информации о наличии товаров на складах
 *
 * @property int $id;
 * @property decimal $cost - Стоимость товара
 * @property int $count - Кол-во товаров на складе
 * @property int $warehouse_id - Склад
 * @property int $product_id - Товар
 * @package frontend\models\ProductInStock\Form
 */
class ProductInStockCreateForm extends Model
{
  /**
   * @var
   */
  public $id;
  /**
   * @var
   */
  public $cost;
  /**
   * @var
   */
  public $count;
  /**
   * @var
   */
  public $warehouse_id;
  /**
   * @var
   */
  public $product_id;

  public function attributeLabels()
  {
    return [
      'cost' => 'Стоимость товара',
      'count' => 'Кол-во штук в наличии',
      'warehouse_id' => 'Склад',
      'product_id' => 'Товар',
    ];

  }

  public function rules()
  {
    return [
      ['cost', 'match', 'pattern' => '/^-?[0-9]+(?:\.[0-9]{1,2})?$/', 'message' => 'Неверный формат стоимости! Пример заполнения: "150.50"'],
      ['count', 'match', 'pattern' => '/^(?!-)[0-9]+$/', 'message' => 'Введите целое положительное число'],
      [['count'], 'number', 'message' => 'Введите числовое значение', 'min' => 0],
      [['warehouse_id', 'product_id', 'id'], 'safe']
    ];
  }

  /**
   * Добавляем в форму данные о складе
   *
   * @param Warehouse $warehouse - Склад
   * @return $this
   */
  public function populateModelToCreate(Warehouse $warehouse) {
    $this->warehouse_id = $warehouse->id;
    return $this;
  }

  /**
   * Добавляем в форму данные из сохраненной записи при обновлении записи
   *
   * @param ProductInStock $product_id_stock - Данные о товаре на складе
   * @return $this
   */
  public function populateModelToUpdate(ProductInStock $product_id_stock) {
    $this->id = $product_id_stock->id;
    $this->cost = $product_id_stock->cost;
    $this->count = $product_id_stock->count;
    $this->warehouse_id = $product_id_stock->warehouse_id;
    $this->product_id = $product_id_stock->product_id;
    return $this;
  }

}