<?php
namespace frontend\models\Site\Form;

use Yii;
use yii\base\Model;
use common\models\User;


/**
 * Форма создания нового пользователя
 *
 * @property $email - Электронная почта
 * @property $password - Пароль
 * @package frontend\models\Site\Form
 */
class SignupForm extends Model
{
    public $email;
    public $password;

    public function attributeLabels()
    {
      return [
        'email' => 'Почта',
        'password' => 'Пароль',
      ];
    }


    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данная почта уже используется'],

            ['password', 'required'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
        ];
    }

  /**
   * Регистрация нового пользователя
   *
   * @return bool|null
   */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        $user = new User();
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        return $user->save();
    }
}
