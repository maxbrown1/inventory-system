<?php
namespace frontend\models\Product;

use Yii;
use yii\base\Exception;
use frontend\models\Product;
use frontend\models\ProductInStock\ProductInStockDeleter;


/**
 * Класс удаления Товара
 *
 * @property Product $product - Товар
 * @property array $product_in_stock - Данные о товарах на складах
 * @package frontend\models\Product
 */
class ProductDeleter
{
  /**
   * @var Product
   */
  protected $product;
  /**
   * @var
   */
  protected $product_in_stock;

  public function __construct(Product $product, $product_in_stock){
    $this->product = $product;
    $this->product_in_stock = $product_in_stock;
  }

  /**
   * Удаление товара и информации о товарах на складах
   *
   * @return $this|bool
   * @throws \Throwable
   */
  public function deleteProduct() {
    $transaction = transaction();
    try
    {
      if(!$this->product->delete()) {
        throw new Exception('Ошибка при удалении товара!');
      }
      foreach ($this->product_in_stock as $product_in_stock) {
        if(!Yii::createObject(ProductInStockDeleter::class, [$product_in_stock])->deleteProductInStock()) {
          throw new Exception('Ошибка при удалении товара на складе!');
        }
      }
      $transaction->commit();
    } catch (Exception $e)
    {
      $transaction->rollBack();
      session()->setFlash('error', $e->getMessage());
      return false;
    }
    return $this;
  }

}