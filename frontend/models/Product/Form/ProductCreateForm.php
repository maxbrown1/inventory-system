<?php
namespace frontend\models\Product\Form;

use frontend\models\Product;
use yii\base\Model;

/**
 * Форма создания Товара
 *
 * @property int $id
 * @property string $name - Наименование товара
 * @property text $description - Описание товара
 * @property date $date_manufacture - Дата изготовления товара
 * @package frontend\models\Product\Form
 */
class ProductCreateForm extends Model
{
  /**
   * @var
   */
  public $id;
  /**
   * @var
   */
  public $name;
  /**
   * @var
   */
  public $description;
  /**
   * @var
   */
  public $date_manufacture;

  public function attributeLabels()
  {
    return [
      'name' => 'Наименование товара',
      'description' => 'Описание товара',
      'date_manufacture' => 'Дата изготовления',
    ];

  }

  public function rules()
  {
    return [
      [['name', 'date_manufacture'], 'required'],
      ['name', 'string', 'max' => 150],
      ['description', 'string', 'max' => 1500],
      ['id', 'safe']
    ];
  }

  /**
   * Наполнение формы данными полученными из сохраненного Товара
   *
   * @param Product $product - Данные товара
   * @return $this
   */
  public function populateModel(Product $product) {
    $this->id = $product->id;
    $this->name = $product->name;
    $this->description = $product->description;
    $this->date_manufacture = $product->date_manufacture;
    return $this;
  }


}