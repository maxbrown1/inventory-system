<?php
namespace frontend\models\Product\Form;

use yii\base\Model;


/**
 * Форма для поиска Товаров
 *
 * @property date $date_begin - Дата начала поиска
 * @property date $date_end - Дата конца поиска
 * @property date $text - Поиск по наименованию товара
 * @package frontend\models\Product\Form
 */
class ProductSearchForm extends Model
{
  /**
   * @var
   */
  public $date_begin;
  /**
   * @var
   */
  public $date_end;
  /**
   * @var
   */
  public $text;

  public function attributeLabels()
  {
    return [
      'date_begin' => 'Дата от',
      'date_end' => 'Дата до',
      'text' => 'Наименование',
    ];

  }

  public function rules()
  {
    return [
      [['date_begin', 'date_end', 'text'], 'required'],
      ['text', 'trim']
    ];
  }

}