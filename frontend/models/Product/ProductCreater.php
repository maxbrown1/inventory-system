<?php
namespace frontend\models\Product;

use Yii;
use yii\base\Exception;
use frontend\models\Helpers\DateHelper;
use frontend\models\Product;
use frontend\models\Product\Form\ProductCreateForm;
use frontend\models\ProductInStock\ProductInStockCreater;


/**
 * Класс создания Товара
 *
 * @property ProductCreateForm $product - форма Товара
 * @property array $product_in_stocks - список Товаров на складе
 * @package frontend\models\Product
 */
class ProductCreater
{
  /**
   * @var ProductCreateForm
   */
  protected $product;
  /**
   * @var
   */
  protected $product_in_stocks;

  public function __construct(ProductCreateForm $product, $product_in_stocks){
    $this->product = $product;
    $this->product_in_stocks = $product_in_stocks;
  }

  /**
   * Сохранение Товара и данных о Товаре на складах
   *
   * @return $this|bool
   */
  public function saveProduct() {
    $transaction = transaction();
    try {
      is_numeric($this->product->id)
        ? $model = Product::findOne([$this->product->id])
        : $model = new Product();
      $model->name = $this->product->name;
      $model->description = $this->product->description;
      $model->date_manufacture = \Yii::createObject(DateHelper::class, [$this->product->date_manufacture])->formatDateForDb();
      if(!$model->save()) {
        throw new Exception('Ошибка при сохранении продукта!');
      }
      if(!Yii::createObject(ProductInStockCreater::class, [$model, $this->product_in_stocks])->saveProductInStock()) {
        throw new Exception('Ошибка при сохранении данных о продукте на складе!');
      }
      $transaction->commit();
    } catch (Exception $e)
    {
      $transaction->rollBack();
      session()->setFlash('error', $e->getMessage());
      return false;
    }
    return $this;
  }


}