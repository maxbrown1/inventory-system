<?php
namespace frontend\models\Product;

use Yii;
use frontend\models\Helpers\DateHelper;
use frontend\models\Product;
use frontend\models\ProductInStock;
use frontend\models\ProductInStock\ProductInStockGetter;
use frontend\models\Product\Form\ProductSearchForm;
use frontend\models\Warehouse\WarehouseGetter;


/**
 * Класс для получения данных по Товару
 *
 * @package frontend\models\Product
 */
class ProductReader
{
  /**
   * Поиск всех активных товаров
   *
   * @return array|bool|\yii\db\ActiveRecord[]
   */
    public function findAllProducts() {
      $products = Product::find()
          ->where(['status' => Product::STATUS_ACTIVE])
          ->orderBy(['date_manufacture'=> SORT_DESC])
          ->all();
      if(!$products) {
        return false;
      }
      return $products;
    }

  /**
   * Получаем массив с товарами и данными по наличию товаров на складах
   *
   * @param $products
   * @return array
   */
    public function getProductsAndProductInStocks($products) {
      $result = [];
      foreach ($products as $product) {
        $result[$product->id]['product'] = $product;
        $product_in_stock = ProductInStock::findAll(['product_id' => $product->id]);
        $result[$product->id]['product_in_stock'] = $product_in_stock;
      }
      return $result;
    }

  /**
   * Поиск товаров по наименованию товара и интервалу дат от и до
   *
   * @param ProductSearchForm $search - данные с формы запроса по поиску товаров
   * @return array|bool|\yii\db\ActiveRecord[]
   * @throws \yii\base\InvalidConfigException
   */
    public function searchProducts(ProductSearchForm $search) {
      $begin = \Yii::createObject(DateHelper::class, [$search->date_begin])->formatDateForDb();
      $end = \Yii::createObject(DateHelper::class, [$search->date_end])->formatDateForDb();
      $products = Product::find()
        ->filterWhere(['like', 'name', $search->text])
        ->andWhere(['>=', 'date_manufacture', $begin])
        ->andWhere(['<=', 'date_manufacture', $end])
        ->orderBy(['date_manufacture'=> SORT_DESC])
        ->all();
      if(!$products) {
        return false;
      }
      return $products;
    }

  /**
   * Все товары и их наличие на складах для view
   *
   * @return array|bool
   */
    public function allProductForView() {
      $products = $this->findAllProducts();
      if(!$products) {
        return false;
      }
      return $this->getProductsAndProductInStocks($products);
    }

  /**
   * Результаты поиска товаров для view
   *
   * @param $search
   * @return array|bool
   * @throws \yii\base\InvalidConfigException
   */
    public function resultSearchProductForView($search) {
      $products = $this->searchProducts($search);
      if(!$products) {
        return false;
      }
      return $this->getProductsAndProductInStocks($products);
    }

  /**
   * Все товары и наличие товаров на складах для API
   *
   * @return array
   */
    public function getAllProductsAndPricesForApi() {
      $products = $this->findAllProducts();
      return $this->apiFormatResponseToProduct($products);
    }

  /**
   * Форматируем данные ответа для API по товарам
   *
   * @param $products
   * @return array
   * @throws \yii\base\InvalidConfigException
   */
    public function apiFormatResponseToProduct($products) {
      $response = [];
      $warehouses = WarehouseGetter::getAllWarehouse();
      foreach ($products as $product) {
        $prod['product']['name'] = $product->name;
        $prod['product']['description'] = $product->description;
        $prod['product']['prices'] = Yii::createObject(ProductInStockGetter::class)->apiFormatProductInStock($product, $warehouses);
        $response[] = $prod;
      }
      return $response;
    }
}











