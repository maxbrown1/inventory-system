<?php
namespace frontend\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;


class ProductInStock extends ActiveRecord
{
  /**
   * @var mixed|null
   */
  private $warehouse_id;
  /**
   * @var mixed|null
   */
  private $product_id;
  /**
   * @var mixed|null
   */
  private $cost;
  /**
   * @var mixed|null
   */
  private $count;
  /**
   * @var mixed|null
   */
  private $id;

  public static function tableName()
  {
    return '{{product_in_stock}}';
  }

  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
        ],
        'value' => new Expression('NOW()'),
      ],
    ];
  }

}