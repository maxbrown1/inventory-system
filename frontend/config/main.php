<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'Система учета товаров',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
          'enablePrettyUrl' => true,
          'showScriptName' => false,
          'enableStrictParsing' => true,
          'rules' => [
            [
              'class' => 'yii\rest\UrlRule',
              'controller' => ['api'],
              'extraPatterns' => [
                'GET product' => 'product',
              ],
              'pluralize' => false
            ],
            '<controller:[\w\-]+>/<id:\d+>/<action:[\w\-]+>' => '<controller>/<action>',
            '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
            '<controller:[\w\-]+>' => '<controller>/index',
            '/' => 'site/index'
          ],
        ],
    ],
    'params' => $params,
];
