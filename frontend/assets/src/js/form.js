$("*[id^=productinstockcreateform-]").change(function(){
  let text = $(this).prop('id');
  let id = parseInt(text.replace(/\D+/g,""));
  if ($(this).val()){
    $('#productinstockcreateform-'+ id +'-count').prop('disabled', false);
  } else {
    $('#productinstockcreateform-'+ id +'-count').prop('disabled', true);
  }
});