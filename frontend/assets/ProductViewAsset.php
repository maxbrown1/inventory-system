<?php
namespace frontend\assets;

use yii\web\AssetBundle;


class ProductViewAsset extends AssetBundle
{

  public $sourcePath = '@frontend/assets/src';

  public $js = [
//    'https://cdn.jsdelivr.net/npm/vue@2',
    'js/form.js',
  ];

  public $depends = [
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset',
    'yii\bootstrap\BootstrapPluginAsset',
  ];
}