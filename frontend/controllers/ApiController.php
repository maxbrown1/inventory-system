<?php
namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;
use frontend\models\Product\ProductReader;

class ApiController extends ActiveController
{
  public $modelClass = 'frontend\models\Product';

  public function actionProduct() {
   return Yii::createObject(ProductReader::class)->getAllProductsAndPricesForApi();
  }
}