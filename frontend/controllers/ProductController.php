<?php
namespace frontend\controllers;

use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use frontend\models\Product;
use frontend\models\Warehouse;
use frontend\models\ProductInStock;
use frontend\models\Product\ProductReader;
use frontend\models\Product\ProductCreater;
use frontend\models\Product\ProductDeleter;
use frontend\models\Product\Form\ProductCreateForm;
use frontend\models\Product\Form\ProductSearchForm;


class ProductController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','new', 'delete', 'update', 'save', 'search'],
        'rules' => [
          [
            'actions' => ['index','new', 'delete', 'update', 'save', 'search'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ]
    ];
  }

  public function actionIndex(){
    $search = Yii::createObject(ProductSearchForm::class);
    $products = Yii::createObject(ProductReader::class)->allProductForView();
    $warehouse_name = ArrayHelper::map(Warehouse::findAll(['status' => Warehouse::STATUS_ACTIVE]), 'id', 'name');
    return ($products)
      ? $this->render('index', ['products' => $products, 'warehouse_name' => $warehouse_name, 'search' => $search])
      : $this->render('not_products');
  }

  public function actionSearch() {
    $search = Yii::createObject(ProductSearchForm::class);
    $warehouse_name = ArrayHelper::map(Warehouse::findAll(['status' => Warehouse::STATUS_ACTIVE]), 'id', 'name');
    if($search->load(request()->post())) {
      performAjaxValidation($search);
      $products = Yii::createObject(ProductReader::class)->resultSearchProductForView($search);
      $this->render('index', ['products' => $products, 'warehouse_name' => $warehouse_name, 'search' => $search]);
    }
    return $this->render('index', ['products' => $products, 'warehouse_name' => $warehouse_name, 'search' => $search]);
  }

  public function actionDelete($id) {
    $product = Product::findOne($id);
    $product_in_stock = ProductInStock::findAll(['product_id' => $product->id]);
    Yii::createObject(ProductDeleter::class, [$product, $product_in_stock])->deleteProduct();
    session()->setFlash('success', "Товарная позиция: $product->name и информация о наличии на складах, удалены из системы");
    return $this->redirect('index');
  }

  public function actionNew(){
    $warehouse_list = Warehouse\WarehouseGetter::getAllWarehouse();
    if(!$warehouse_list) {
      session()->setFlash('danger', "Невозможно добавить товар, пока недобавлен склад");
      return $this->redirect('index');
    }
    $product = Yii::createObject(ProductCreateForm::className());
    $product_in_stocks = Yii::createObject(ProductInStock\ProductInStockGetter::class)->getAllModelsForCreateForm($warehouse_list);
    return $this->render('view', ['product' => $product, 'product_in_stocks' => $product_in_stocks, 'warehouse_list' => $warehouse_list]);
  }

  public function actionUpdate($id) {
    $product = Yii::createObject(ProductCreateForm::className())->populateModel(Product::findOne($id));
    $warehouse_list = Warehouse\WarehouseGetter::getAllWarehouse();
    $product_in_stocks = Yii::createObject(ProductInStock\ProductInStockGetter::class)->getAllModelsForUpdateForm($warehouse_list, ProductInStock::findAll(['product_id' => $product->id]));
    return $this->render('view', ['product' => $product, 'product_in_stocks' => $product_in_stocks, 'warehouse_list' => $warehouse_list]);
  }

  public function actionSave($id=false) {
    $warehouse_list = Warehouse\WarehouseGetter::getAllWarehouse();
    if($id === false) {
      $product = Yii::createObject(ProductCreateForm::className());
      $product_in_stocks = Yii::createObject(ProductInStock\ProductInStockGetter::class)->getAllModelsForCreateForm($warehouse_list);
    }else {
      $product = Yii::createObject(ProductCreateForm::className())->populateModel(Product::findOne($id));;
      $product_in_stocks = Yii::createObject(ProductInStock\ProductInStockGetter::class)->getAllModelsForUpdateForm($warehouse_list, ProductInStock::findAll(['product_id' => $product->id]));
    }
    if(Model::loadMultiple($product_in_stocks, Yii::$app->request->post()) && $product->load(request()->post())) {
      $isValid = $product->validate();
      if($isValid) {
        performAjaxMultipleValidation($product_in_stocks);
        Yii::createObject(ProductCreater::class, [$product, $product_in_stocks])->saveProduct();
        return $this->redirect('index');
      }
    }
    return $this->render('view', ['product' => $product, 'product_in_stocks' => $product_in_stocks, 'warehouse_list' => $warehouse_list]);
  }
}