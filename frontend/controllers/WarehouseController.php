<?php
namespace frontend\controllers;


use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use frontend\models\Warehouse;
use frontend\models\Warehouse\WarehouseCreater;
use frontend\models\Warehouse\WarehouseDeleter;
use frontend\models\Warehouse\Form\WarehouseCreateForm;


class WarehouseController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','new','save','delete'],
        'rules' => [
          [
            'actions' => ['index','new','save','delete'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ]
    ];
  }

  public function actionIndex()
  {
    $warehouses = Warehouse::findAll(['status' => Warehouse::STATUS_ACTIVE]);
    if($warehouses){
      return $this->render('index', ['warehouses' => $warehouses]);
    }
    return $this->render('not_warehouse');
  }

  public function actionNew()
  {
    $model = new WarehouseCreateForm();
    if($model->load(request()->post())) {
      performAjaxValidation($model);
      Yii::createObject(WarehouseCreater::class, [$model])->createWarehouse();
      return $this->redirect('/warehouse/index');
    }
    return $this->render('new', ['model' => $model]);
  }

  public function actionDelete($id) {
    $warehouse = Warehouse::findOne($id);
    Yii::createObject(WarehouseDeleter::class, [$warehouse])->delete();
    session()->setFlash('success', "Склад: $warehouse->name и данные о товарах на данном складе удалены!");
    return $this->redirect('/warehouse/index');
  }


}