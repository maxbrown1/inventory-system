<?php

/* @var $this yii\web\View */

$this->title = 'Главная';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Здравствуйте!</h1>
        <br><br>
        <p class="lead">Вы авторизовались в системе под логином: <strong><?=$user->email;?></strong></p>
        <p>Можете приступать к работе.</p>
        <br><br>
    </div>

</div>
