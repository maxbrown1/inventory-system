<?php

/* @var $this yii\web\View */

$this->title = 'Главная';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Поздравляем!</h1>
        <br>
        <p class="lead">Вы попали в систему учета товаров на складах.</p>
        <p>Для дальнейшей работы авторизуйтесь или пройдите регистрацию новой учетной записи.</p>
        <br><br>
      <div class="row">
        <div class="btn-group btn-group-justified">
          <a class="btn btn-lg btn-success" href="/site/login">Авторизация</a>
          <a class="btn btn-lg btn-primary" href="/site/signup">Регистрация</a>
        </div>
      </div>
    </div>

</div>
