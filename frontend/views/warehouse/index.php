<?php
/* @var $this yii\web\View */
/* @var $warehouses yii\web\View */

use yii\helpers\Html;

$this->title = 'Склады';
?>

  <div class="row">
    <div class="col-sm-8">
      <div class="h3">Список складов</div>
    </div>
    <div class="col-sm-4">
      <a href="/warehouse/new" class="btn btn-primary h3 btn-block">Добавить склад</a>
    </div>
    <br><br><br><br>
  </div>


  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover table-bordered">
        <thead>
          <th>Код склада</th>
          <th>Наименование склада</th>
          <th>Редактирование</th>
        </thead>
        <tbody>
          <?php
          foreach ($warehouses as $warehouse) {
            echo Html::beginTag('tr');
              echo Html::tag('td', Html::encode($warehouse->code));
              echo Html::tag('td', Html::encode($warehouse->name));
              echo Html::beginTag('td');
                echo Html::a('Удалить', "/warehouse/{$warehouse->id}/delete", ['class' => 'color-link-journal']);
              echo Html::endTag('td');
              echo Html::endTag('tr');
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>

</div>
