<?php

/* @var $this yii\web\View */

$this->title = 'Товары';
?>
<div class="col-sm-12">

  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-8">
        <div class="h3">Список складов</div>
      </div>
      <a class="col-sm-4">
        <a href="/warehouse/new" class="btn btn-primary h3">Добавить склад</a>
    </div>
  </div>
  <br><br>
</div>

  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-8">
        <p class="h4">Склады не найдены!</p>
      </div>
    </div>
  </div>

</div>
