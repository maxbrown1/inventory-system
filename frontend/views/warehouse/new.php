<?php
/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Новый склад';
?>
  <div class="col-sm-12">

    <div class="col-sm-6 offset-3">
      <div class="h3">Новый склад</div>
      <br>
      <p>Заполните данные о складе:</p>
      <br>
      <?php

      $form = ActiveForm::begin(
        [
          'enableAjaxValidation' => true,
          'enableClientValidation' => false,
        ]);
      echo $form->field($model, 'code');
      echo $form->field($model, 'name');
      ?>

      <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
      </div>
      <?php ActiveForm::end(); ?>

    </div>

  </div>
</div>
