<?php
/* @var $this yii\web\View */
/* @var $product yii\web\View */
/* @var $product_in_stocks yii\web\View */
/* @var $warehouse_list yii\web\View */

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use frontend\assets\ProductViewAsset;

ProductViewAsset::register($this);

$this->title = 'Новый товар';
?>
<div class="col-sm-12">

    <div class="h3">Новый товар</div>
    <br>
    <p>Заполните данные о товаре:</p>
    <br>
    <?php
    $date = ($product->id)
     ? $date = new \DateTime($product->date_manufacture)
     : $date = new \DateTime();
    $date = $date->format('d.m.Y');

    $form = ActiveForm::begin(
      [
        'action' => ($product->id) ?'/product/'.$product->id.'/save' : '/product/save',
        'id' => 'create-product-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
      ]);

    echo $form->field($product, 'name');
    echo $form->field($product, 'description');
    echo $form->field($product, 'date_manufacture')
      ->widget(DatePicker::classname(), [
        'readonly' => true,
        'options' => [
            'value' => $date,
        ],
        'pluginOptions' => [
          'autoclose'=>true
        ]
    ]);

    ?>
        <table class="table table-hover table-bordered">
          <thead>
          <th>Наименование склада</th>
          <th>Стоимость, руб.</th>
          <th>Кол-во штук в наличии</th>
          </thead>
          <tbody>
          <?php
          foreach ($product_in_stocks as $index => $product_in_stock) {
            $disabled = ($product_in_stock->cost) ? false : true;
            echo Html::beginTag('tr');
              echo Html::tag('td', Html::encode($warehouse_list[$product_in_stock->warehouse_id]->name));
              echo Html::beginTag('td');
                echo $form->field($product_in_stock, "[$index]cost")->label(false);
              echo Html::endTag('td');
              echo Html::beginTag('td');
                echo $form->field($product_in_stock, "[$index]count")->textInput(['disabled' => $disabled])->label(false);
              echo Html::endTag('td');
            echo Html::endTag('tr');
          }
          ?>
          </tbody>
        </table>

    <div class="form-group">
      <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
</div>
