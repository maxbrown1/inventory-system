<?php
/* @var $this yii\web\View */
/* @var $search yii\web\View */
/* @var $products yii\web\View */
/* @var $warehouse_name yii\web\View */

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use frontend\models\Helpers\DateHelper;
use frontend\models\Helpers\TextHelper;

$this->title = 'Товары';
?>

  <div class="row">
      <div class="col-sm-8">
        <div class="h3">Список товаров</div>
      </div>
      <div class="col-sm-4">
        <a href="/product/new" class="btn btn-primary h3 btn-block">Добавить товар</a>
      </div>
    <br><br><br><br>
  </div>

  <?php
  $form = ActiveForm::begin(
    [
      'id'=>'search-form',
      'class' => 'form-inline',
      'action' => '/product/search',
      'enableAjaxValidation' => true,
      'enableClientValidation' => false,
    ]);
  ?>

  <div class="row">

    <div class="col-sm-6">
      <?php
      echo '<label class="control-label">Поиск по периоду</label>';
      echo DatePicker::widget([
        'model' => $search,
        'attribute' => 'date_begin',
        'attribute2' => 'date_end',
        'options' => ['placeholder' => 'Дата от'],
        'options2' => ['placeholder' => 'Дата до'],
        'type' => DatePicker::TYPE_RANGE,
        'form' => $form,
        'pluginOptions' => [
          'format' => 'dd.mm.yyyy',
          'autoclose' => true,
        ]
      ]);
      ?>
    </div>

    <div class="col-sm-4">
      <?php
      echo $form->field($search, 'text');
      ?>
    </div>

    <div class="col-sm-2 form-group">
     <label class="control-label">&nbsp;</label>
      <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary btn-block']) ?>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover table-bordered">
        <thead>
        <th>Дата изготовления</th>
        <th>Наименование товара</th>
        <th>Описание товара</th>
        <th>Склад и стоимость</th>
        <th>Редактировать</th>
        </thead>
        <tbody>
        <?php
        if($products) {
          foreach ($products as $key => $product) {
            echo Html::beginTag('tr');
            echo Html::tag('td', \Yii::createObject(DateHelper::class, [$product['product']->date_manufacture])->formatDateHumanView());
            echo Html::tag('td', Html::encode($product['product']->name));
            echo Html::tag('td', \Yii::createObject(TextHelper::class, [$product['product']->description])->cutterText(50));
            echo Html::beginTag('td');
            foreach ($product['product_in_stock'] as $product_in_stock) {
              if($product_in_stock->cost !== null) {
                echo Html::tag('p', \Yii::createObject(TextHelper::class, [$product_in_stock->cost])->warehouseAndPriceProduct($warehouse_name[$product_in_stock->warehouse_id]));
              }
            }
            echo Html::endTag('td');
            echo Html::beginTag('td');
            echo Html::a('Редактировать', "/product/{$product['product']->id}/update", ['class' => 'color-link-journal']);
            echo '<br>';
            echo Html::a('Удалить', "/product/{$product['product']->id}/delete", ['class' => 'color-link-journal']);
            echo Html::endTag('td');
            echo Html::endTag('tr');
          }
        }else {
          echo '<tr>';
            echo '<td colspan="5" class="text-center">';
            echo '<p class="h3">По вашему запросу ничего не найдено!</p>';
            echo '</td>';
          echo '</tr>';
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>

</div>
