<?php

/* @var $this yii\web\View */

$this->title = 'Товары';
?>
<div class="col-sm-12">

  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-8">
        <div class="h3">Список товаров</div>
      </div>
      <a class="col-sm-4">
        <a href="/product/new" class="btn btn-primary h3">Добавить товар</a>
      </div>
    </div>
    <br><br>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-8">
        <p class="h4">Товары не найдены!</p>
      </div>
    </div>
  </div>

</div>
